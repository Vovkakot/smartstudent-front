// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  ssr:false,
  dir:{
    pages:'pages'
  },
  css: ["@/assets/styles/main.scss","@/assets/styles/input.css"],
  app: {
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      title: 'SmartStudent',
      meta:[
      ]
    }
  },
  modules: [
    '@pinia/nuxt',
  ],
  // runtimeConfig: {
  //   public: {
  //     baseURL: process.env.BASE_URL || 'http://localhost:5001',
  //   },
  // },
})
