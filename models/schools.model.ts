export interface schoolsItem {
    "Id": number,
    "Template": number,
    "Number": number,
    "ShortName": string,
    "FullName": string,
    "Address": string
}

export interface universityItem {
    "Id": number,
    "Template": number,
    "ShortName": string,
    "FullName": string,
    "Description":string,
    "Address":string,
    "Professions": []
}
 export interface professionItem {
     "Id": number,
     "Template": number,
     "Name": string,
     "Description": string,
     "DateCreated": string,
     "ProfessionType": {
         "Id": number,
         "Template": number,
         "Name":string,
         "Description": string
     },
     "Universities": universityItem[]
 }
