import {defineStore} from "pinia"
import {professionItem} from "~/models/schools.model";

export type State = {
    professions: professionItem[]
}
export const useProfessionsStore = defineStore('professionsStore', {
    state: (): State => ({
        professions: []
    } as State),
    actions: {
        async fetchProfessionsList(): Promise<void> {
            const {data} = await useFetch<professionItem[]>('http://localhost:5001/profession');
            if (data.value) {
                this.professions = data.value;
            }
        }
    },
    getters: {
        getProfessions: (state: State): professionItem[] => {
            return state.professions
        }
    }
})
