import {defineStore} from "pinia"
import {universityItem} from "~/models/schools.model";

export type State = {
    universities: universityItem[]
}
export const useUniversitiesStore = defineStore('universityStore', {
    state: (): State => ({
        universities: []
    } as State),
    actions: {
        async fetchUniversitiesList(): Promise<void> {
            const {data} = await useFetch<universityItem[]>('http://localhost:5001/university');
            if (data.value) {
                this.universities = data.value;
            }
        }
    },
    getters: {
        getUniversities: (state: State): universityItem[] => {
            return state.universities
        }
    }
})
