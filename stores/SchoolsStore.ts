import {defineStore} from "pinia"
import {schoolsItem} from "~/models/schools.model";

export type State = {
    schools: schoolsItem[],
    school: schoolsItem
}
export const useSchoolsStore = defineStore('schoolStore', {
    state: (): State => ({
        schools: [],
        school: {}
    } as unknown as State),
    actions: {
        async fetchSchoolsList(): Promise<void> {
            const {data} = await useFetch<schoolsItem[]>('http://localhost:5001/school');
            if (data.value) {
                this.schools = data.value;
            }
        },
        async fetchOneSchools(id: number): Promise<void> {
            const {data} = await useFetch<schoolsItem>(`http://localhost:5001/school/${id}`);
            if (data.value) {
                this.school = data.value;
            }
        },
        async editSchool(dataObj: object) {
            const requestOptions = {
                method: 'PATCH',
                headers: {},
                body: JSON.stringify(dataObj),
            };
            const {data, error} = useFetch(`http://localhost:5001/school/${dataObj.id}`, requestOptions);
        }
    },
    getters: {
        getSchools: (state: State): schoolsItem[] => {
            return state.schools
        },
        getOneSchool: (state: State): schoolsItem => {
            return state.school
        }
    }
})
